resource "aws_instance" "student1" {
  ami                    = var.AMI_ID
  instance_type          = "t3.micro"
  subnet_id              = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNETS[0]
  vpc_security_group_ids = [aws_security_group.allow_ssh_and_http.id]
  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-app"
  }

# File Provisioner, to copy the file to the remote machine
provisioner "file" {
  source      = "/var/lib/jenkins/.ssh/id_rsa"
  destination = "/home/centos/.ssh/id_rsa"

  connection {
    type        = "ssh"
    user        = "centos"
    private_key = file("/var/lib/jenkins/.ssh/id_rsa")
    host        = self.private_ip
  }
}
   # Connection Provisioner
    connection {
      type        = "ssh"
      user        = "centos"
      private_key = file("/var/lib/jenkins/.ssh/id_rsa")
      host        = self.private_ip
    }

 # Remote Provisioner
    provisioner "remote-exec" {
      inline = [
        "echo -e 'Host *\n\tStrictHostKeyChecking no' > /home/centos/.ssh/config && chmod 600 /home/centos/.ssh/config /home/centos/.ssh/id_rsa",
        "sudo yum install git ansible -y",
        "ansible-pull -U git@gitlab.com:clouddevops-b48/ansible.git stack-pull.yml -e DBNAME=studentapp -e DBUSERNAME=student -e DBPASSWORD=student1"
      ]
    }
}


output "instance_ip_address" {
    value = "${aws_instance.student1.private_ip}"
}

resource "local_file" "host_file" {
    content  = "${aws_instance.student1.private_ip}"
    filename = "/tmp/dynamic-hosts"
}

# output "VPC-ID" {
#     value = data.terraform_remote_state.vpc.outputs.VPC_ID
# }