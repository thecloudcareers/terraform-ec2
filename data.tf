# Used to read the data from a remote statefile, here in this case, reading from terraform-vpc state file
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
        bucket         = var.BUCKET
        key            = "${var.PROJECTNAME}/vpc/${var.ENV}/terraform.tfstate"
        region         =  var.region
    }
}